package me.yamalidon.languageapi;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public final class LanguageAPI extends JavaPlugin {

    Config config;

    @Override
    public void onEnable() {
        config = new Config(this,"config.yml");
        config.build();
        if (config.getConfig().getBoolean("check-updates")) {
            getServer().getScheduler().runTaskLaterAsynchronously(this, () -> {
                Updater updater = new Updater(54081);
                try {
                    if (updater.checkForUpdates()) {
                        getLogger().warning("LanguageAPI has a new version!");
                    }
                } catch (Exception e) {
                    getLogger().warning("Could not check for updates! Stacktrace:");
                    e.printStackTrace();
                }
            }, 1L);
        }
    }

    public static LanguageAPI getPlugin() {
        return (LanguageAPI) Bukkit.getPluginManager().getPlugin("LanguageAPI");
    }

}
