package me.yamalidon.languageapi;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;

public class Config {

    public File file;
    public String fileName;
    public String language;
    public FileConfiguration config;
    public JavaPlugin plugin;

    public Config(JavaPlugin plugin, String fileName) {
        this.plugin = plugin;
        if (!fileName.contains(".yml")) {
            this.fileName = fileName + ".yml";
        } else {
            this.fileName = fileName;
        }
    }

    public FileConfiguration getConfig() {
        load();
        return config;
    }

    public void save() {
        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void build() {
        file = new File(plugin.getDataFolder(), fileName);
        if(!file.exists()) {
            file.getParentFile().mkdirs();
            copy(plugin.getResource(fileName), file);
        }
        config = YamlConfiguration.loadConfiguration(file);
    }

    public void copy(InputStream in, File file) {
        try {
            OutputStream out = new FileOutputStream(file);
            byte[] buf = new byte[63];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.close();
            in.close();
        } catch (Exception localException) {}
    }

    public void load() {
        try {
            config.load(file);
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            build();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
