package me.yamalidon.languageapi;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class LanguageFile extends Config {

    private HashMap<String, List<Config>> languageFiles = new HashMap<>();
    private HashMap<String, List<Config>> sameFiles = new HashMap<>();
    private List<String> languages = new ArrayList<>();
    private String baseLanguage;

    public LanguageFile(JavaPlugin plugin, String filename) {
        super(plugin, filename);
    }

    public void addLanguageFile(String language,Config config) {
        if (languageFiles.get(language) != null) {
            List<Config> tmp = languageFiles.get(language);
            tmp.add(config);
            config.setLanguage(language);
            languageFiles.put(language,tmp);
        }
    }

    public HashMap<String, List<Config>> getLanguageFiles() {
        return languageFiles;
    }

    public List<String> getLanguages() {
        return languages;
    }

    public void setLanguages(List<String> languages) {
        this.languages = languages;
    }

    public void addLanguage(String language) {
        languages.add(language);
        languageFiles.put(language,new ArrayList<>());
    }

    public void removeLanguage(String language) {
        languages.remove(language);
    }

    public void setSameFile(String type, Config... config) {
        sameFiles.put(type, Arrays.asList(config));
    }

    public Config getSameFile(String type) {
        if (sameFiles.get(type) != null) {
            for (Config config : sameFiles.get(type)) {
                if (config.getLanguage().equalsIgnoreCase(baseLanguage)) {
                    return config;
                }
            }
        }
        return null;
    }

    public String getBaseLanguage() {
        return baseLanguage;
    }

    public void setBaseLanguage(String baseLanguage) {
        this.baseLanguage = baseLanguage;
    }

    public void buildLanguageFiles() {
        for (String language : languages) {
            new File(plugin.getDataFolder()+"/"+language).mkdir();
            for (Config config : languageFiles.get(language)) {
                build(language,config);
            }
        }
    }

    public void build(String language, Config tmpConfig) {
        tmpConfig.file = new File(plugin.getDataFolder()+"/"+language, tmpConfig.fileName);
        if(!tmpConfig.file.exists()) {
            tmpConfig.file.getParentFile().mkdirs();
            copy(plugin.getResource(language+"/"+tmpConfig.fileName), tmpConfig.file);
        }
        tmpConfig.config = YamlConfiguration.loadConfiguration(tmpConfig.file);
    }

}
